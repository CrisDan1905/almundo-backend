#  Sobre la aplicación

* Esta es una aplicación creada con express
* Se utilizó la libreria `dotenv` para manejar las variables de entorno en produccion y en desarrollo
* Se creó una base de datos `MongoDB` en la plataforma MLab
* Para la conexión con la base de datos se utilizó `Mongoose`
* Se crea CRUD de hoteles
* Se crearon dos colecciones en la base de datos con la siguiente estructura:  

    +-- hotels: Coleccion de hoteles. Contiene un id que hace referencia a su respectiva imagen  

    +-- images: Colección de imagenes guardadas en base64    

* Existen dos archivos para el manejo de variables de entorno para iniciar la app en producción o en desarrollo. Se encuentran en la carpeta `environment`.  

  +-- development.env: Variables de entorno de producción.  
  +-- development.env: Variables de entorno de desarrollo.  

* La estructura de un hotel a guardar es la siguiente:

        {
            "stars": number,
            "amenities": [String],
            "name": String,
            "price": Number,
            "image": {
                "data": <imagen en base64>,
                "contentType": "image/<tipo de la imagen>"
            }
        }

# Antes de iniciar la aplicación
Cuando se vaya a iniciar la aplicación por primera vez se debe usar el comando `npm install`. Este instalará todas las dependencias necesarias para que el proyecto funcione

# Cómo iniciar la aplicación en entorno de desarrollo

Se usa el comando `npm start`

# Cómo iniciar la aplicación en entorno de producción

Se usa el comando `npm run dev`