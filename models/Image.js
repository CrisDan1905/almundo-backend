const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const ImageSchema = new mongoose.Schema({
    data: String,
    contentType: String
});

module.exports = mongoose.model('Image', ImageSchema)