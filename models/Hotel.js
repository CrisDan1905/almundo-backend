const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const HotelSchema = new mongoose.Schema({
    name: {
        type: String,
        required: 'Por favor, provea un nombre para el hotel'
    },
    stars: {
        type: Number,
        max: 5,
        min: 0,
        default: 0
    },
    price: {
        type: Number,
        required: 'Por favor, provea un precio para el hotel'
    },
    image: {
        type: mongoose.Schema.ObjectId,
        ref: 'Image'
    },
    amenities: [String],
    // type: Map,
    // of: String,

});

function autoPopulate(next) {
    this.populate('image');
    next();
}

HotelSchema.pre('find', autoPopulate);

module.exports = mongoose.model('Hotel', HotelSchema);
