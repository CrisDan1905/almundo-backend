function setQuerySearch(value) {
    switch((value).constructor.name) {
        case 'Array':
            return { $in: value }
            break;
        case 'String':
            return new RegExp(value, 'ig');
            break;
        default:
            return value
    }
}

function generateImgs() {
    const dir = fs.readdirSync(__dirname + '/../assets/images/hotels');
    dir.forEach(async (filename, indx) => {
        const fileRead = fs.readFileSync(__dirname + '/../assets/images/hotels/' + filename)
        const buffImg = new Buffer.from(fileRead).toString('base64');
        const img = await Image.create({
            data: buffImg,
            contentType: 'image/jpg'
        })
        imgIds.push(img.id)
        req.body[indx].image = img.id
        await Hotel.create(req.body[indx]);
        console.log(img.id)
    })
}


module.exports = {
    setQuerySearch
}