const catchErrors = (fn) => function(req, res, next) {
    return fn(req, res, next).catch(err => { console.log(err); res.status(400).send({err}); });
};

module.exports = {
    catchErrors
};