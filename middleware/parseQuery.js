function parseQuery(req, res, next) {

    res.locals.parsedQuery = {};
    Object.keys(req.query).forEach(key => res.locals.parsedQuery[key] = parseFunction(req.query[key]));

    next();
}

function parseFunction(value) {
    if (/(^\[|\]$)/gm.test(value))
        return value.replace(/(^\[|\]$)/gm, '').split(',').map(val => parseFunction(val));
    else if (value === 'true')
        return true;
    else if (value === 'false')
        return false;
    else if (+value)
        return +value
    else
        return value;
}

module.exports = parseQuery