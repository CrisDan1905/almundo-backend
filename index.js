let envVariables;

if (process.env.NODE_ENV==='production')
    envVariables = './environment/production.env';
else
    envVariables = './environment/development.env';

require('dotenv').config({ path: envVariables });


const mongoose = require('mongoose');

const app = require('./app');

app.set('port', process.env.PORT || 3000);

app.get('/', (req, res) => res.send('Hello World!'));

// connection to DB
mongoose.Promise = global.Promise;
mongoose.connect(process.env.DATABASE)
    .then(() => console.log('Conexión con base de datos establecida'))
    .catch(err => console.error('Error al intentar conectarse con la base de datos'));

app.listen(app.get('port'), () => 
  console.log(`app corriendo en ${process.env.URI}:${app.get('port')}`)
);