const express = require('express');
const router = express.Router();

const { catchErrors } = require('../handlers/errorHandler');
const parseQuery = require('../middleware/parseQuery');
const hotelController = require('../controllers/HotelController');

router.get('/hotels', parseQuery, catchErrors(hotelController.getHotels));
router.post('/hotel', catchErrors(hotelController.createHotel));
router.put('/hotel', catchErrors(hotelController.updateHotel));
router.delete('/hotel/:id', catchErrors(hotelController.deleteHotel));
module.exports = router;