const Hotel = require('../models/Hotel');
const Img = require('../models/Image');
const { setQuerySearch } = require('../handlers/utils');

const getHotels = async (req, res) => {
    const response = {};
    response.data = await Hotel.find(setQuery(res.locals.parsedQuery));
    response.info = 'Datos enviados correctamente';
    res.status(200).send(response);
}

const createHotel = async (req, res) => {
    const createdImg = await Img.create(req.body.image);
    const creation = await Hotel.create({...req.body, image: createdImg._id});
    res.status(200).send({info: 'Creación exitosa', data: creation});
}

const updateHotel = async (req, res) => {
    const updated = await Hotel.findByIdAndUpdate(req.body._id, req.body, {new: true});
    res.status(200).send({info: 'edición exitosa', data: updated});
}

const deleteHotel = async (req, res) => {
    const { id } = req.params;

    const HotelToDelete = await Hotel.findById(id);
    await Img.deleteOne({_id: HotelToDelete.image});
    const deletedHotel = await Hotel.deleteOne({_id: id});

    res.status(200).send({info: 'eliminación exitosa'})
    
}

function setQuery(queryString) {
    const queryObj = {};
    const queryKeys = Object.keys(queryString);
    if (queryKeys.length)
        queryObj.$and = queryKeys.map(key => ({[key]: setQuerySearch(queryString[key])}));
    
    return queryObj

}


module.exports = {
    getHotels,
    createHotel,
    updateHotel,
    deleteHotel
};